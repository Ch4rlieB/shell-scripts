for f in *.srt;
do
	echo "Working with file $f"
        iconv -f CP1250 -t UTF-8 "$f" > "$f-UTF8"
done
