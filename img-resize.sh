#!/bin/bash
# ~/Scripts/img-resize.sh twitter-card.png 100
# $1 name of the file
# $2 width

file_name=$1
width=$2

if [[ $file_name == *.png ]]; then
  file_name_thumb=$(echo $file_name | cut -d '.' -f 2 --complement)
  extension="px.png"
  file_name_thumb="$file_name_thumb-$width$extension"
  #echo "konci na .png a nove jmeno souboru bude $file_name_thumb"
  times="x99999"
  echo "Converting (convert) image $file_name to width $width"
  convert $file_name -resize $width$times -strip $file_name_thumb
  echo "Quality optimalization (pngquant)"
  pngquant -o $file_name_thumb --force --quality=70-80 $file_name_thumb
  echo "Optimalization (optipng)"
  optipng $file_name_thumb
fi
