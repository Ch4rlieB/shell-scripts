# Shell scripts which I sometimes use
## img-resize.sh
Resize and optimize PNG image
## img-optimize.sh
Optimize PNG image
## subtitles-cp1250-utf8.sh
Converts subtitles from CP1250 encoding to UTF-8
